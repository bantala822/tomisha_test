import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tomisha_test/common/styles/app_colors.dart';
import 'package:tomisha_test/common/styles/static_decoration.dart';
import 'package:tomisha_test/common/styles/textstyle.dart';
import 'package:tomisha_test/features/home/cubit/home_cubit.dart';
import 'package:tomisha_test/features/home/view/web/widgets/arbeitgeber_desktop_view.dart';
import 'package:tomisha_test/features/home/view/web/widgets/arbeitnehmer_desktop_view.dart';
import 'package:tomisha_test/features/home/view/web/widgets/tempor%C3%A4rb%C3%BCro_desktop_view.dart';
import 'package:tomisha_test/features/home/view/widget/button.dart';
import 'package:tomisha_test/features/home/view/widget/tabbar.dart';

class HomeDesktopView extends StatelessWidget {
  const HomeDesktopView({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          _headerView(),
          height10,
          commonTabBar(),
          customHeight(20),
          BlocBuilder<HomeCubit, HomeState>(
            builder: (context, state) {
              return [
                const ArbeitnehmerDesktopView(),
                const ArbeitgeberDesktopView(),
                const TemporarburoDesktopView(),
              ][state.index];
            },
          ),
          customHeight(20),
        ],
      ),
    );
  }

  Widget _headerView() {
    return ClipPath(
      clipper: WaveClipperTwo(),
      child: Container(
        height: 500,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              appPrimaryColor.withOpacity(0.3),
              appTextColor.withOpacity(0.3),
            ],
          ),
        ),
        child: IntrinsicHeight(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                flex: 2,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Deine Job\nwebsite",
                      style: AppTextStyle.regularBold36
                          .copyWith(fontSize: 65, color: appBlackColor),
                    ),
                    height10,
                    commonButton()
                  ],
                ),
              ),
              Flexible(
                child: ConstrainedBox(
                  constraints:
                      const BoxConstraints(maxWidth: 300, maxHeight: 300),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(300),
                    child: Container(
                      color: appWhiteColor,
                      child: SvgPicture.asset(
                        "assets/svg/undraw_agreement_aajr.svg",
                        fit: BoxFit.cover,
                        // alignment: Alignment.topCenter,
                      ),
                    ),
                  ),
                ),
              ),
              const Expanded(
                flex: 2,
                child: SizedBox(),
              )
            ],
          ),
        ),
      ),
    );
  }
}
