import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tomisha_test/common/styles/app_colors.dart';
import 'package:tomisha_test/common/styles/textstyle.dart';
import 'package:tomisha_test/features/home/cubit/home_cubit.dart';

Widget commonTabBar() {
  return BlocBuilder<HomeCubit, HomeState>(
    builder: (context, state) {
      final tabs = context.read<HomeCubit>().tabBarList;
      return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        child: SizedBox(
          height: 50,
          child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              children: [
                ...List.generate(
                  tabs.length,
                  (index) => GestureDetector(
                    onTap: () {
                      context.read<HomeCubit>().changeTabIndex(index);
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.horizontal(
                          left: Radius.circular(index == 0 ? 12 : 0),
                          right: Radius.circular(index == 2 ? 12 : 0),
                        ),
                        border:
                            Border.all(color: Colors.grey.shade300, width: 1),
                        color: state.index == index
                            ? tabBarColor
                            : Colors.transparent,
                      ),
                      height: 40,
                      width: 160,
                      child: Center(
                        child: Text(
                          tabs[index],
                          style: TextStyle(
                              color: state.index == index
                                  ? selectedTabIndex
                                  : appTextColor),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      );
    },
  );
}
