import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tomisha_test/common/styles/textstyle.dart';
import 'package:tomisha_test/features/home/cubit/home_cubit.dart';
import 'package:tomisha_test/features/home/view/mobile/views/arbeitgeber_view.dart';
import 'package:tomisha_test/features/home/view/mobile/views/arbeitnehmer_view.dart';
import 'package:tomisha_test/features/home/view/mobile/views/tempor%C3%A4rb%C3%BCro_view.dart';
import 'package:tomisha_test/features/home/view/widget/tabbar.dart';

class HomeMobileView extends StatelessWidget {
  const HomeMobileView({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          SizedBox(height: 67.h),
          _headerView(),
          commonTabBar(),
          BlocBuilder<HomeCubit, HomeState>(
            builder: (context, state) {
              return [
                const ArbeitnehmerView(),
                const ArbeitgeberView(),
                const TemporarburoView(),
              ][state.index];
            },
          ),
          SizedBox(height: 10.h),
        ],
      ),
    );
  }

  Widget _headerView() {
    return ClipPath(
        clipper: WaveClipperTwo(),
        child: Container(
          height: 600,
          decoration: BoxDecoration(
              // gradient: LinearGradient(
              //   colors: [
              //     appPrimaryColor.withOpacity(0.2),
              //     appTextColor.withOpacity(0.2),
              //   ],
              // ),
              ),
          child: Column(children: [
            Container(
              constraints: BoxConstraints(maxWidth: 200),
              padding:
                  const EdgeInsets.only(top: 20.0, left: 20.0, right: 20.0),
              child: Text(
                'Deine Job website',
                style:
                    TextStyle(fontWeight: FontWeight.w500, fontSize: 30.spMin),
                textAlign: TextAlign.center,
              ),
            ),
            SvgPicture.asset(
              "assets/svg/undraw_agreement_aajr.svg",
              height: 400,
              fit: BoxFit.cover,
            ),
          ]),
        ));
  }
}
