import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:tomisha_test/features/home/cubit/home_cubit.dart';
import 'package:tomisha_test/features/home/view/mobile/home_mobile_view.dart';
import 'package:tomisha_test/features/home/view/web/home_desktop_view.dart';
import 'package:tomisha_test/features/home/view/widget/button.dart';
import 'package:tomisha_test/helper/responsive.dart';

class HomeView extends StatelessWidget {
  const HomeView({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => HomeCubit(),
      child: Scaffold(
        extendBodyBehindAppBar: true,
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(67.h),
            child: SafeArea(
              child: Column(
                children: [
                  const AppBarLine(),
                  Container(
                    height: 62.h,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                            color: Colors.grey.shade300,
                            offset: Offset(0, 2.h),
                            blurRadius: 10.h)
                      ],
                      borderRadius:
                          BorderRadius.vertical(bottom: Radius.circular(20.h)),
                    ),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 20.w),
                            child: Text('Login',
                                style: TextStyle(
                                    fontSize: 17.spMin,
                                    color: const Color(0xff319795))),
                          )
                        ]),
                  )
                ],
              ),
            )),
        bottomNavigationBar: (Responsive.isDesktop(context))
            ? null
            : Container(
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.vertical(
                    top: Radius.circular(12),
                  ),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black12,
                      offset: Offset(-1, 0),
                      spreadRadius: 3,
                      blurRadius: 6,
                    )
                  ],
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    SizedBox(height: 20.h),
                    commonButton(),
                    SizedBox(height: 20.h),
                  ],
                ),
              ),
        body: const Responsive(
            mobile: HomeMobileView(), desktop: HomeDesktopView()),
      ),
    );
  }
}

class AppBarLine extends StatelessWidget {
  const AppBarLine({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 5.h,
      width: double.infinity,
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          colors: [
            Color(0xff319795),
            Color(0xff3182CE),
          ],
        ),
      ),
    );
  }
}
