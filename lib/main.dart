import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:tomisha_test/features/home/view/home.dart';
import 'package:tomisha_test/helper/responsive.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
        designSize: Responsive.isDesktop(context)
            ? const Size(1920, 1080)
            : const Size(360, 640),
        builder: (context, _) {
          return MaterialApp(
            theme: ThemeData(fontFamily: 'Lato'),
            home: const HomeView(),
          );
        });
  }
}
